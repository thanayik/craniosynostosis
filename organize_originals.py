#!/usr/bin/env fslpython
import os
import glob
import json
import sys
from fsl.wrappers import fast, fslmaths
from fsl.utils import path as fslpath
from fsl.data.image import ALLOWED_EXTENSIONS
import nibabel as nii

def rename(image_path):
    new_path = image_path
    if '(' in image_path:
        new_path = image_path.replace('(', '')
    if ')' in new_path:
        new_path = new_path.replace(')', '')
    if '&' in new_path:
        new_path = new_path.replace('&', '_')

    if new_path == image_path:
        return new_path
    else:
        os.rename(image_path, new_path)
    return new_path


def remove_useless_images(start_dir):
    '''find all the CT images we want to work with'''
    files = glob.glob(os.path.join(os.path.abspath(start_dir), '**/*.nii.gz'), recursive=True)
    files = sorted(files)
    for file in files:
        img =nii.load(file)
        dims = img.header['pixdim']
        jsonfile = os.path.abspath(file).replace('.nii.gz', '.json') 
        # if json file does not exist, then it is a derived file from a previous run, deleted it now to cleanup
        if not os.path.exists(jsonfile):
            os.unlink(file)
            continue
        with open(jsonfile, 'r') as jfile:
            jcontents = jfile.read()
        jdata = json.loads(jcontents)
        seriesD = jdata['SeriesDescription']

        if dims[3] >= 1:
            fullpath = os.path.abspath(file)
            os.unlink(fullpath)
            os.unlink(fullpath.replace('.nii.gz', '.json'))
        elif 'Bone' in seriesD:
            fullpath = os.path.abspath(file)
            os.unlink(fullpath)
            os.unlink(fullpath.replace('.nii.gz', '.json'))
        elif 'Hr60' in seriesD:
            fullpath = os.path.abspath(file)
            os.unlink(fullpath)
            os.unlink(fullpath.replace('.nii.gz', '.json'))

        rename(jsonfile)
        rename(file)

def main():
  remove_useless_images(sys.argv[1])

if __name__ == "__main__":
    main()
